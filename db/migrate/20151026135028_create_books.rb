class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
			t.string :book_code
			t.string :title
			t.string :class_no
			t.string :subject
			t.string :author
			t.string :publisher
			t.string :yop
			t.string :city
			t.string :language
			t.references :book_type
			t.float :price
			t.string :physic_data
			t.integer :borrow_amount
			t.integer :read_amount
			t.integer :ref_amount
			t.integer :exptotal
			t.string :isbn
			t.string :resource
      t.timestamps null: false
    end
    add_index :books, :book_type_id
    add_index :books, :book_code
  end
end
