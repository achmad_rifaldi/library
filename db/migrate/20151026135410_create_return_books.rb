class CreateReturnBooks < ActiveRecord::Migration
  def change
    create_table :return_books do |t|
			t.references :borrow_book
			t.references :user
			t.date :return_date
			t.float :total_fine
      t.timestamps null: false
    end
    add_index :return_books, :borrow_book_id
    add_index :return_books, :user_id
  end
end
