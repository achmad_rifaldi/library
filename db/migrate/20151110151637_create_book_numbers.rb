class CreateBookNumbers < ActiveRecord::Migration
  def change
    create_table :book_numbers do |t|
      t.string	:book_type_id
      t.string	:year
      t.integer	:current_number, default: 0
      t.timestamps
    end
  end
end
