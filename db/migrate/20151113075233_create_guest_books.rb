class CreateGuestBooks < ActiveRecord::Migration
  def change
    create_table :guest_books do |t|
      t.string :name
      t.string :from
      t.string :necessary
      t.text :suggestion
      t.datetime :date
      t.timestamps
    end
  end
end
