class AddReturnStatusToBorrowBooks < ActiveRecord::Migration
  def change
  	add_column :borrow_books, :return_status, :boolean, default: false
  end
end
