class CreateTheses < ActiveRecord::Migration
  def change
    create_table :theses do |t|
			t.string :thesis_code
			t.string :year
			t.string :title
			t.string :category
			t.string :author
			t.string :guider1
			t.string :guider2
			t.text :description
			t.boolean :media, default: false
      t.timestamps null: false
    end
    add_index :theses, :thesis_code
  end
end
