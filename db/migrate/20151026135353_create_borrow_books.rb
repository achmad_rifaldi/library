class CreateBorrowBooks < ActiveRecord::Migration
  def change
    create_table :borrow_books do |t|
			t.string :borrow_code
			t.references :user
			t.references :book
      t.references :member
			t.date :borrow_date
			t.date :return_date
      t.timestamps null: false
    end
    add_index :borrow_books, :user_id
    add_index :borrow_books, :book_id
    add_index :borrow_books, :borrow_code
    add_index :borrow_books, :member_id
  end
end
