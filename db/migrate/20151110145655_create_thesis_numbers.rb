class CreateThesisNumbers < ActiveRecord::Migration
  def change
    create_table :thesis_numbers do |t|
      t.string	:kind
      t.string	:year
      t.string	:category
      t.integer	:current_number, default: 0
      t.timestamps
    end
  end
end
