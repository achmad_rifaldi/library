class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
    	t.string :nim
		t.string :firstname
		t.string :lastname
		t.string :pob
		t.date :dob
		t.string :gender
		t.string :address
		t.string :phone
		t.date :active_period
		t.boolean :status, default: false
      t.timestamps null: false
    end
    add_index :members, :nim
  end
end
