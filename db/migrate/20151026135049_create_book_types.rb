class CreateBookTypes < ActiveRecord::Migration
  def change
    create_table :book_types do |t|
    	t.string :name
		t.string :borrow_time
		t.float :fine_per_day
      t.timestamps null: false
    end
  end
end
