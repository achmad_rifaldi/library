# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151113075233) do

  create_table "book_numbers", force: true do |t|
    t.string   "book_type_id"
    t.string   "year"
    t.integer  "current_number", default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "book_types", force: true do |t|
    t.string   "name"
    t.string   "borrow_time"
    t.float    "fine_per_day", limit: 24
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "books", force: true do |t|
    t.string   "book_code"
    t.string   "title"
    t.string   "class_no"
    t.string   "subject"
    t.string   "author"
    t.string   "publisher"
    t.string   "yop"
    t.string   "city"
    t.string   "language"
    t.integer  "book_type_id"
    t.float    "price",         limit: 24
    t.string   "physic_data"
    t.integer  "borrow_amount"
    t.integer  "read_amount"
    t.integer  "ref_amount"
    t.integer  "exptotal"
    t.string   "isbn"
    t.string   "resource"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "attachment"
  end

  add_index "books", ["book_code"], name: "index_books_on_book_code", using: :btree
  add_index "books", ["book_type_id"], name: "index_books_on_book_type_id", using: :btree

  create_table "borrow_books", force: true do |t|
    t.string   "borrow_code"
    t.integer  "user_id"
    t.integer  "book_id"
    t.integer  "member_id"
    t.date     "borrow_date"
    t.date     "return_date"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "return_status", default: false
  end

  add_index "borrow_books", ["book_id"], name: "index_borrow_books_on_book_id", using: :btree
  add_index "borrow_books", ["borrow_code"], name: "index_borrow_books_on_borrow_code", using: :btree
  add_index "borrow_books", ["member_id"], name: "index_borrow_books_on_member_id", using: :btree
  add_index "borrow_books", ["user_id"], name: "index_borrow_books_on_user_id", using: :btree

  create_table "guest_books", force: true do |t|
    t.string   "name"
    t.string   "from"
    t.string   "necessary"
    t.text     "suggestion"
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "members", force: true do |t|
    t.string   "nim"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "pob"
    t.date     "dob"
    t.string   "gender"
    t.string   "address"
    t.string   "phone"
    t.date     "active_period"
    t.boolean  "status",        default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "members", ["nim"], name: "index_members_on_nim", using: :btree

  create_table "return_books", force: true do |t|
    t.integer  "borrow_book_id"
    t.integer  "user_id"
    t.date     "return_date"
    t.float    "total_fine",     limit: 24
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "return_books", ["borrow_book_id"], name: "index_return_books_on_borrow_book_id", using: :btree
  add_index "return_books", ["user_id"], name: "index_return_books_on_user_id", using: :btree

  create_table "theses", force: true do |t|
    t.string   "thesis_code"
    t.string   "year"
    t.string   "title"
    t.string   "category"
    t.string   "author"
    t.string   "guider1"
    t.string   "guider2"
    t.text     "description"
    t.boolean  "media",       default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "kind"
  end

  add_index "theses", ["thesis_code"], name: "index_theses_on_thesis_code", using: :btree

  create_table "thesis_numbers", force: true do |t|
    t.string   "kind"
    t.string   "year"
    t.string   "category"
    t.integer  "current_number", default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "avatar"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
