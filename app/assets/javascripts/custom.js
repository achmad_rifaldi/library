var CallContent = function(uri) {
	$("#FormContainer").modal('show');

	$.ajax({
	    beforeSend: function(){
	      $("#FormContainer #model-content").html("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
	    },
	    url: uri,
	    cache: false,
	    success: function(data){
	      setTimeout(function(){
	        $("#FormContainer #model-content").html(data);
	      }, 5000);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	      // $('.loader img').hide();
	      // $('.error').show();
	      // $('.message-error').html(thrownError);
	    }
	});
}

var CallDetail = function(class_name) {
	var DetailElement = $('.detail-'+class_name);
	var BtnDetailElement = $('.btn-detail-'+class_name);

	if( DetailElement.is(":visible") ) {
		DetailElement.addClass('hide');
		BtnDetailElement.text('Detail');
	}
	else {
		DetailElement.removeClass('hide');
		BtnDetailElement.text('Less');
	}
}

$(function () {
  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
  });

  //Initialize Select2 Elements
  $(".select2").select2();

  $("[data-mask]").inputmask();

  $("#AddBtn").click(function(){
    $("#Add").modal('show');
  });

  // Member Area
  $("#member_dob").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});

  $('body').on('change', '#borrow_book_member_id', function() {
  	var member_id = $(this).val();
  	$.ajax({
	    beforeSend: function(){
	    	$('#history').html("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
	    },
	    url: '/borrow_books/validate_borrow_books?member_id='+member_id,
	    cache: false,
	    success: function(data){
	      // $("#FormContainer #model-content").html(data);
	      // setTimeout(function(){
	      //   $('.loader').hide();
	      // }, 5000);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {}
	});
  });

  $('body').on('change', '#borrow_book_book_id', function() {
  	var book_id = $(this).val();
  	$.ajax({
	    beforeSend: function(){},
	    url: '/borrow_books/validate_return_date?book_id='+book_id,
	    cache: false,
	    success: function(data){
	    	console.log(data.return_date);
	    	$('#borrow_book_return_date').val(data.return_date);
	    },
	    error: function (xhr, ajaxOptions, thrownError) {}
	});
  });
});