// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require vendor/jquery-ui.min
//= require vendor/bootstrap.min
//= require vendor/modal
//= require vendor/daterangepicker
//= require vendor/bootstrap-datepicker
//= require vendor/bootstrap3-wysihtml5.all.min
//= require vendor/jquery.slimscroll.min
//= require vendor/fastclick.min
//= require vendor/icheck.min
//= require vendor/jquery.inputmask
//= require vendor/jquery.inputmask.date.extensions
//= require vendor/jquery.inputmask.extensions
//= require vendor/select2.full.min
//= require vendor/jquery.flot.min
//= require vendor/jquery.flot.resize.min
//= require vendor/jquery.flot.pie.min
//= require vendor/jquery.flot.categories
//= require vendor/app.min
//= require custom
