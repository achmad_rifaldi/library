class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy, :generate_barcode]
  before_action :set_book_type, only: [:index, :edit]

  def index
    @books = Book.all.order('id desc').page(params[:page]).per(params[:per_page])
    @book = Book.new
  end

  def search
    @books = Book.searching(params[:keywords])
  end

  def generate_barcode
    @book.generate_barcode

    redirect_to "/downloads/barcode/books/#{@book.isbn}.png"
  end

  def new
    @book = Book.new
  end

  def edit
    render layout: false
  end

  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to books_url, notice: 'Buku berhasil ditambahkan.' }
      else
        format.html { redirect_to books_url, notice: 'Buku tidak berhasil ditambahkan.' }
      end
    end
  end

  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to books_url, notice: 'Buku berhasil dirubah.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, notice: 'Buku berhasil dihapus.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    def set_book_type
      @book_types = BookType.all.map {|type| [type.name, type.id] }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:book_code, :title, :class_no, :subject, :author, :publisher, :yop, :city, :language, :book_type_id, :price, :attachment, :borrow_amount, :read_amount, :ref_amount, :exptotal, :isbn, :resource)
    end
end