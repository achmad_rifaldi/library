class ThesesController < ApplicationController
  before_action :set_thesis, only: [:show, :edit, :update, :destroy]

  def index
    @theses = Thesis.all.order('id desc').page(params[:page]).per(params[:per_page])
    @thesis = Thesis.new
  end

  def search
    @theses = Thesis.searching(params[:keywords])
  end

  def show;end

  def new
    @thesis = Thesis.new
  end

  def edit
    render layout: false
  end

  def create
    @thesis = Thesis.new(thesis_params)

    respond_to do |format|
      if @thesis.save
        format.html { redirect_to theses_url, notice: "#{@thesis.kind} berhasil dibuat." }
        # format.json { render :show, status: :created, location: @thesis }
      else
        format.html { render :new }
        # format.json { render json: @thesis.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @thesis.update(thesis_params)
        format.html { redirect_to theses_url, notice: "#{@thesis.kind} berhasil diubah." }
        # format.json { render :show, status: :ok, location: @thesis }
      else
        format.html { render :edit }
        # format.json { render json: @thesis.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    kind = @thesis.kind
    @thesis.destroy
    respond_to do |format|
      format.html { redirect_to theses_url, notice: "#{kind} berhasil dihapus." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thesis
      @thesis = Thesis.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def thesis_params
      params.require(:thesis).permit(:thesis_code, :year, :title, :category, :kind, :author, :guider1, :guider2, :description, :media)
	end
end
