class BookTypesController < ApplicationController
	before_action :set_book_type, only: [:show, :edit, :update, :destroy]

  def index
    @book_types = BookType.all.page(params[:page]).per(params[:per_page])
    @book_type = BookType.new
  end

  def search
    @book_types = BookType.searching(params[:keywords])
  end

  def new
    @book_type = BookType.new
  end

  def edit
    render layout: false
  end

  def create
    @book_type = BookType.new(book_params)

    respond_to do |format|
      if @book_type.save
        format.html { redirect_to book_types_url, notice: 'Tipe Buku berhasil dibuat.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @book_type.update(book_params)
        format.html { redirect_to book_types_url, notice: 'Tipe Buku berhasil dirubah.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @book_type.destroy
    respond_to do |format|
      format.html { redirect_to book_types_url, notice: 'Tipe Buku berhasil dihapus.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book_type
      @book_type = BookType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book_type).permit(:name, :borrow_time, :fine_per_day)
    end
end
