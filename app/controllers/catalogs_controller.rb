class CatalogsController < ApplicationController
	skip_before_filter :authenticate_user!

	layout 'catalog'

	def index
		@guest_book = GuestBook.new
		@guest_books = GuestBook.of_day
	end

	def search
		if params[:category] == "Book"
			@books = Book.searching(params[:keywords])
		else
			@theses = Thesis.searching(params[:keywords])
		end
	end
end
