class BorrowBooksController < ApplicationController
  before_action :set_borrow_book, only: [:show, :edit, :update, :destroy]
  before_action :set_books, only: [:index, :edit]
  before_action :set_members, only: [:index, :edit]

  def index
    @borrow_books = BorrowBook.where(return_status: false).page(params[:page]).per(params[:per_page]).order('return_date ASC')
    @borrow_book = BorrowBook.new
  end

  def search
    @borrow_books = BorrowBook.searching(params[:keywords])
  end

  def validate_borrow_books
    @borrow_books = BorrowBook.where("member_id = ? AND return_status = false", params[:member_id])
  end

  def validate_return_date
    book = Book.where(id: params[:book_id]).first
    borrow_time = !book.blank? ? book.book_type.borrow_time : 5
    return_date = Time.now + (borrow_time.to_i).days

    render json: { return_date: return_date }
  end

  def new
    @borrow_book = BorrowBook.new
  end

  # def edit
  #   render layout: false
  # end

  def create
    @borrow_book = BorrowBook.new(borrow_book_params)

    respond_to do |format|
      if @borrow_book.save
        format.html { redirect_to borrow_books_url, notice: 'Peminjaman berhasil dibuat.' }
      else
        format.html { render :new }
      end
    end
  end

  # def update
  #   respond_to do |format|
  #     if @borrow_book.update(borrow_book_params)
  #       format.html { redirect_to borrow_books_url, notice: 'Peminjaman berhasil dirubah.' }
  #     else
  #       format.html { render :edit }
  #     end
  #   end
  # end

  # def destroy
  #   @borrow_book.destroy
  #   respond_to do |format|
  #     format.html { redirect_to borrow_books_url, notice: 'Peminjaman berhasil dihapus.' }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_borrow_book
      @borrow_book = BorrowBook.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def borrow_book_params
       params.require(:borrow_book).permit(:borrow_code, :user_id, :book_id, :member_id, :borrow_date, :return_date)
    end

    def set_books
    	@books = Book.all.map {|book| ["#{book.book_code} - #{book.title}", book.id] }
    end

    def set_members
    	@members = Member.where(status: true).map {|member| ["#{member.nim} - #{member.firstname} #{member.lastname}", member.id] }
    end
end
