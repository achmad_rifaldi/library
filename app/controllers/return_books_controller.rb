class ReturnBooksController < ApplicationController
  before_filter :set_borrow_book, except: [:index, :search]

  def index
    @borrow_books = BorrowBook.where(return_status: true).order('updated_at desc').all.page(params[:page]).per(params[:per_page])
  end

  def search
    @borrow_books = BorrowBook.joins(:return_book).searching(params[:keywords])
  end

  def new
    @return_book = ReturnBook.new
    render layout: false
  end

  def show
  	render layout: false
  end

  def create
    @return_book = ReturnBook.new(return_book_params)
    @return_book.borrow_book.update_attribute(:return_status, true)

    respond_to do |format|
      if @return_book.save
        format.html { redirect_to borrow_books_url, notice: "Peminjaman Buku #{@return_book.borrow_book.book.name} berhasil dikembalikan." }
      else
        format.html { render :new }
      end
    end
  end

  private
  	# Use callbacks to share common setup or constraints between actions.
    def set_borrow_book
      @borrow_book = BorrowBook.find(params[:borrow_book_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def return_book_params
       params.require(:return_book).permit(:borrow_book_id, :user, :return_date, :total_fine)
    end
end
