class MembersController < ApplicationController
	before_action :set_member, only: [:show, :edit, :update, :destroy, :generate_card]
	before_action :gender_option, only: [:index, :edit]
  before_action :prepare_barcode, only: [:generate_card]

  def index
    @members = Member.all.order('id desc').page(params[:page]).per(params[:per_page])
    @member = Member.new
  end

  def search
    @members = Member.searching(params[:keywords])
  end

  def download
    generate_file = Member.import_all
    redirect_to "/downloads/daftar_anggota.xls"
  end

  def generate_card
    html = render_to_string template: "members/card.html.erb", layout: "pdf", locals: { member: @member}
    pdf = WickedPdf.new.pdf_from_string(html)
    save_path = Rails.root.join('public', 'downloads', 'pdfs',"#{@member.nim}.pdf")
    File.open(save_path, 'wb') do |file|
      file << pdf
    end

    redirect_to "/downloads/pdfs/#{@member.nim}.pdf"
  end

  def new
    @member = Member.new
  end

  def edit
    render layout: false
  end

  def create
    @member = Member.new(member_params)

    respond_to do |format|
      if @member.save
        format.html { redirect_to members_url, notice: 'Anggota berhasil ditambahkan.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @member.update(member_params)
        format.html { redirect_to members_url, notice: 'Anggota berhasil diubah.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @member.destroy
    respond_to do |format|
      format.html { redirect_to members_url, notice: 'Anggota berhasil dihapus.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    def prepare_barcode
      @member.generate_barcode
    end

    # Never trust parameters from the scary internet, only allow the white list through.
  def member_params
      params.require(:member).permit(:nim, :firstname, :lastname, :pob, :dob, :gender, :address, :phone, :active_period, :status)
	end

	def gender_option
		@gender = [['Laki-Laki', 'L'],['Perempuan', 'P']]
	end
end
