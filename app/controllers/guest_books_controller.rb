class GuestBooksController < ApplicationController
  before_action :set_book, only: [:destroy]

  def index
    @guest_books = GuestBook.all.order('id desc').page(params[:page]).per(params[:per_page])
  end

  def search
    @guest_books = GuestBook.searching(params[:keywords])
  end

  def create
    @guest_book = GuestBook.new(guest_book_params)

    respond_to do |format|
      if @guest_book.save
        format.html { redirect_to root_url, notice: 'Data berhasil ditambahkan.' }
      else
        format.html { redirect_to books_url, notice: 'Data tidak berhasil ditambahkan.' }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def guest_book_params
      params.require(:guest_book).permit(:name, :from, :necessary, :suggestion, :date)
    end
end
