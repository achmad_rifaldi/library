module ApplicationHelper
	def amount_fine(borrow_book)
		if Time.now > borrow_book.return_date
			(Time.now.to_date - borrow_book.return_date.to_date).to_i * borrow_book.book.book_type.fine_per_day
		else
			0
		end
	end

	def late(borrow_book)
		if Time.now > borrow_book.return_date
			(Time.now.to_date - borrow_book.return_date.to_date).to_i
		else
			0
		end
	end

	def amount_borrow_books
		borrow_books = Array.new

		BorrowBook.all.map {|bb| borrow_books << bb if bb.return_book.blank? }

		return borrow_books.count
	end

	def count_current_book(book)
		borrowed = BorrowBook.where('book_id = ? AND return_status = true', book.id).count
		book.exptotal - borrowed
	end
end
