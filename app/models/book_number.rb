class BookNumber < ActiveRecord::Base
	def self.generate_number(object)
		# set class name of data to be manipulated
		class_name = object.class.to_s

	    type = object.book_type_id
	    year = object.yop? ? object.yop : Time.now.year

	    conditions =  ["book_type_id = ? AND year = ?", type, year]

	    # Find Or Create
	    book_number = self.where(conditions).first || self.create!({book_type_id: type, year: year})
	    # Increase form number
	    current_number = book_number.current_number + 1

	    # Create Static Length Number i.e 0001
	    number = current_number.to_s.split('')
	    number.map { |value|
	      number.unshift(0) if number.count < 4
	    }

	    book_type = type.to_s.split('')
	    book_type.map { |value|
	      book_type.unshift(0) if book_type.count < 2
	    }

	    if object.update_attribute(:book_code, "#{book_type.join('')}#{year}#{number.join('')}") #set form number
	      # update current number
	      book_number.update_attribute(:current_number, current_number)
	    end
	end
end
