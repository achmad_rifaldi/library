class Book < ActiveRecord::Base
	mount_uploader :attachment, AttachmentUploader

	belongs_to	:book_type

	has_many :borrow_books

	scope :searching, lambda{ |query| where("book_code like :keyword OR title like :keyword OR author like :keyword OR publisher like :keyword OR yop like :keyword OR resource like :keyword", { keyword: "%#{query}%" }) unless query.blank? }

	validates :isbn, length: { minimum: 13, maximum: 13 }

	after_create :prepare_number

	def prepare_number
		BookNumber.generate_number(self)
	end

	def generate_barcode
		require 'barby/barcode/code_128'
		require 'barby/outputter/cairo_outputter'

		barcode = Barby::Code128B.new(self.isbn)
		save_path = Rails.root.join('public', 'downloads', 'barcode', 'books',"#{self.isbn}.png")
		File.open(save_path, 'w'){|f| f.write barcode.to_png }
	end
end
