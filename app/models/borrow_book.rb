class BorrowBook < ActiveRecord::Base
	MAXBOOK = 2

	belongs_to :member
	belongs_to :user
	belongs_to :book

	has_one :return_book

	scope :searching, lambda{ |query| where(borrow_code: query) }
end
