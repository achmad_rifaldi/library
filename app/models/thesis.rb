class Thesis < ActiveRecord::Base
	CATEGORY = [["RPL", "RPL"],["SI", "SI"],["APLIKASI", "APLIKASI"],["SIMULASI", "SIMULASI"]]
	KIND = [["TUGAS AKHIR", "TA"],["LAPORAN KERJA PRAKTEK", "LKP"]]

	scope :searching, lambda{ |query| where("thesis_code like :keyword OR year like	:keyword OR title like :keyword OR category like :keyword", { keyword: "%#{query}%" }) unless query.blank? }

	after_create :prepare_number

	def prepare_number
		ThesisNumber.generate_number(self)
	end
end
