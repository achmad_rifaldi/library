class GuestBook < ActiveRecord::Base
	scope :of_day, lambda { where("YEAR(date) = '#{Time.now.year}' AND MONTH(date) = '#{Time.now.month}' AND DAY(date) = '#{Time.now.day}'") }
	scope :searching, lambda{ |query| where("`name` like :keyword OR `from` like :keyword OR `necessary` like :keyword", { keyword: "%#{query}%" }) }
end
