class ThesisNumber < ActiveRecord::Base
	def self.generate_number(object)
		# set class name of data to be manipulated
		class_name = object.class.to_s

	    kind = object.kind
	    year = object.year
	    category = object.category

	    conditions =  ["kind = ? AND year = ? AND category = ?", kind, year, category]

	    # Find Or Create
	    thesis_number = self.where(conditions).first || self.create!({kind: kind, year: year, category: category})
	    # Increase form number
	    current_number = thesis_number.current_number + 1

	    # Create Static Length Number i.e 0001
	    number = current_number.to_s.split('')
	    number.map { |value|
	      number.unshift(0) if number.count < 4
	    }

	    if object.update_attribute(:thesis_code, "#{kind}/#{year}/#{category}/#{number.join('')}") #set form number
	      # update current number
	      thesis_number.update_attribute(:current_number, current_number)
	    end
	end
end
