class Member < ActiveRecord::Base
	CONTAINER_DIR = File.join(Rails.public_path, "downloads")

	has_many :borrow_books

	scope :searching, lambda{ |query| where("nim like :keyword OR firstname like :keyword OR lastname like :keyword", { keyword: "%#{query}%" }) unless query.blank? }

	def self.import_all
		unless File.directory? CONTAINER_DIR #~> check if container dir exist
	      Dir.mkdir CONTAINER_DIR, 0755
	    end

		require 'spreadsheet'

		Spreadsheet.client_encoding = 'UTF-8'

		members_file = Spreadsheet::Workbook.new
		members_file_sheet1 = members_file.create_worksheet

		members_file_sheet1[0,0] = "NIM"
		members_file_sheet1[0,1] = "Nama"
		members_file_sheet1[0,2] = "Tempat Lahir"
		members_file_sheet1[0,3] = "Tanggal Lahir"
		members_file_sheet1[0,4] = "Jenis Kelamin"
		members_file_sheet1[0,5] = "Alamat"
		members_file_sheet1[0,6] = "Telepon"
		members_file_sheet1[0,7] = "Masa Aktif"
		members_file_sheet1[0,8] = "Status"

		index = 1
		Member.all.each do |member|
			members_file_sheet1[index,0] = member.nim
			members_file_sheet1[index,1] = "#{member.firstname} #{member.lastname}"
			members_file_sheet1[index,2] = member.pob
			members_file_sheet1[index,3] = member.dob
			members_file_sheet1[index,4] = member.gender
			members_file_sheet1[index,5] = member.address
			members_file_sheet1[index,6] = member.phone
			members_file_sheet1[index,7] = member.active_period
			members_file_sheet1[index,8] = member.status? ? "Aktif" : "Non Aktif"

			index += 1
		end

		path_file = File.join(CONTAINER_DIR, "daftar_anggota.xls")
		members_file.write path_file
	end

	def generate_barcode
		require 'barby/barcode/code_128'
		require 'barby/outputter/cairo_outputter'

		barcode = Barby::Code128B.new(self.nim)
		save_path = Rails.root.join('public', 'downloads', 'barcode', 'members',"#{self.nim}.png")
		File.open(save_path, 'w'){|f| f.write barcode.to_png }
	end
end
