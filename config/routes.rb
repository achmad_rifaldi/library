Rails.application.routes.draw do
  devise_for :users
  root 'catalogs#index'

  resources :catalogs, only: [:index] do
    # collection do
    #   post 'guest_books'
    # end
  end

  resources :guest_books do
    collection do
      get 'search'
    end
  end

  resources :book_types do
    collection do
      get 'search'
    end
  end

  resources :books do
    member do
      get 'generate_barcode'
    end
    collection do
      get 'search'
    end
  end

  resources :catalogs do
    collection do
      get 'search'
    end
  end

  resources :dashboard, only: [:index]

  resources :theses do
    collection do
      get 'search'
    end
  end

  resources :members do
    member do
      get 'generate_card'
    end
    collection do
      get 'search'
      get 'download'
    end
  end

  resources :return_books, only: [:index] do
    collection do
      get 'search'
    end
  end

  resources :borrow_books, only: [:index, :new, :create] do
    resources :return_books

    collection do
      get 'search'
      get 'validate_borrow_books'
      get 'validate_return_date'
    end
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
